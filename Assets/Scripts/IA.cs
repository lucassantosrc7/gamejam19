﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Attack))]
public class IA : MonoBehaviour
{
    Transform player;

    enum State {
        Nothing, Persue, Wait
    }

    State state = State.Nothing;

    Transform[] places;

    [SerializeField]
    float speed = 5;

    [SerializeField]
    float distance = 2, distanceBack = 2;

    Transform target;

    Animator anim;

    Rigidbody rb;

    Attack attack;
    EventTriggerFunctions buttonAtk;

    float time;

    [SerializeField]
    float timeAtk;

    AudioSource source;

    DetectHit detectHit;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        source = GetComponent<AudioSource>();
        detectHit = GetComponent<DetectHit>();

        places = new Transform[GameController.scenario.hidingPlace.childCount];

        for(int i = 0; i < GameController.scenario.hidingPlace.childCount; i++)
        {
            places[i] = GameController.scenario.hidingPlace.GetChild(i);
        }

        Transform place = places[Random.Range(0, places.Length)];
        Vector3 v3 = place.position;
        v3.y = transform.position.y;

        transform.position = v3;
        transform.rotation = place.rotation;
        state = State.Persue;

        attack = GetComponent<Attack>();

        buttonAtk = new EventTriggerFunctions();
        buttonAtk.state = EventTriggerFunctions.Functions.Null;

        attack.functions = buttonAtk;
        attack.anim = anim;
        attack.source = source;

        player = GameController.player.transform;
        target = player.transform;

        GameObject[] Huds = GameController.Instance.hudNPC;
        for (int i = 0; i < Huds.Length; i++)
        {
            if (Huds[i].CompareTag(gameObject.tag))
            {
                Huds[i].SetActive(true);
                detectHit.text = Huds[i].GetComponentInChildren<UnityEngine.UI.Text>();
            }
            else
            {
                Huds[i].SetActive(false);
            }
        }
        detectHit.source = source;
    }

    void Update()
    {
        switch (state){
            case (State.Persue):
                RaycastHit hit;
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance))
                {
                    if (hit.transform.CompareTag("bufalo") || hit.transform.CompareTag("cavalo"))
                    {
                        if (state != State.Wait)
                        {
                            buttonAtk.state = EventTriggerFunctions.Functions.Down;
                            time = Time.time + timeAtk;
                            state = State.Wait;
                        }
                    }
                    else
                    {
                        target = places[Random.Range(0, places.Length)];
                        state = State.Persue;
                    }
                }

                Vector3 relativePos = target.position - transform.position;

                // the second argument, upwards, defaults to Vector3.up
                Vector3 euler = transform.eulerAngles;
                Quaternion rot = Quaternion.LookRotation(relativePos, Vector3.up);
                euler.y = rot.eulerAngles.y;

                anim.SetBool("Walk", true);

                rb.rotation = Quaternion.Euler(euler);
                rb.MovePosition(transform.position + transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime);

                if(Vector3.Distance(transform.position, target.position) <= 1.8f)
                {
                    target = player.transform;
                }
                break;
            case (State.Wait):
                anim.SetBool("Walk", false);
                if (Time.time > time)
                {
                    target = places[Random.Range(0, places.Length)];
                    state = State.Persue;
                }
                break;
        }
    }
}
