﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horned : MonoBehaviour
{
    [SerializeField]
    Attack attack;

    [SerializeField]
    float multiply = 3;

    private void OnTriggerEnter(Collider hit)
    {
        object[] objs = new object[] { (attack.distanceBack * 3), ForceMode.Impulse };
        hit.transform.SendMessage("Hit", objs, SendMessageOptions.DontRequireReceiver);
    }
}
