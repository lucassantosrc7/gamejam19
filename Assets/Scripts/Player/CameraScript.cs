﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    Transform player;

    [SerializeField]
    float distance, heigth;

    void Start()
    {
        player = GameController.player.transform;
    }

    void Update()
    {
        Vector3 camPos = player.position;
        camPos.y += heigth;
        camPos += Vector3.back * distance;

        transform.position = camPos;
        transform.LookAt(player);
    }
}
