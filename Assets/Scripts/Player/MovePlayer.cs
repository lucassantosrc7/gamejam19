﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePlayer : MonoBehaviour
{
    GameController gameController;
    FixedJoystick moveJoystick;

    [Space(20)]
    [SerializeField]
    float speed = 5f;

    Transform circle;

    [SerializeField]
    float circleBorder;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        gameController = GameController.Instance;

        circle = gameController.circle;
        moveJoystick = gameController.buttonMove.GetComponent<FixedJoystick>();
    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0)
        && (gameController.buttonAtk.state == EventTriggerFunctions.Functions.Exit   || gameController.buttonAtk.state == EventTriggerFunctions.Functions.Null)
        && (gameController.buttonSkill.state == EventTriggerFunctions.Functions.Exit || gameController.buttonSkill.state == EventTriggerFunctions.Functions.Null))
        {
            Player.anim.SetBool("Walk", true);

            circle.localPosition = new Vector2(moveJoystick.Direction.x * circleBorder, moveJoystick.Direction.y * circleBorder);

            Quaternion q = Quaternion.LookRotation(circle.position, Vector3.forward);
            Vector3 euler = Vector3.zero;
            euler.y = q.eulerAngles.y;

            rb.rotation = Quaternion.Euler(euler);
            rb.MovePosition(transform.position + transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime);
        }
        else
        {
            Player.anim.SetBool("Walk", false);
        }
    }
}