﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class DetectHit : MonoBehaviour
{
    Rigidbody rb;

    [SerializeField]
    [Range(0,1)]
    float decreasePercent;

    float percent = 0;

    [SerializeField]
    AudioClip[] clipsDamage;

    [HideInInspector]
    public AudioSource source;

    [HideInInspector]
    public UnityEngine.UI.Text text;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(text != null)
        {
            text.text = (percent * 100).ToString() + "%";
        }
    }

    void Hit(object[] objs)
    {
        if (clipsDamage.Length > 0 && !source.isPlaying)
        {
            source.PlayOneShot(clipsDamage[Random.Range(0, clipsDamage.Length)]);
        }

        float dis = (float)objs[0] * (1 + percent);
        rb.AddForce(transform.TransformDirection(Vector3.back) * dis, (ForceMode)objs[1]);

        percent += decreasePercent;
    }
}
