﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    EventTriggerFunctions buttonAtk, buttonMove, buttonSkill;

    public static Animator anim;

    float animYPos;

    Attack attack;

    AudioSource source;

    DetectHit detectHit;

    void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        animYPos = anim.transform.localPosition.y;
        attack = GetComponent<Attack>();
        source = GetComponent<AudioSource>();
        detectHit = GetComponent<DetectHit>();
    }

    void Start()
    {
        GameController gameController = GameController.Instance;

        GameObject[] Huds = GameController.Instance.hudPlayer;
        for(int i = 0; i < Huds.Length; i++)
        {
            if (Huds[i].CompareTag(gameObject.tag))
            {
                Huds[i].SetActive(true);
                detectHit.text = Huds[i].GetComponentInChildren<UnityEngine.UI.Text>();
            }
            else
            {
                Huds[i].SetActive(false);
            }
        }

        detectHit.source = source;

        buttonAtk = gameController.buttonAtk;
        buttonMove = gameController.buttonMove;
        buttonSkill = gameController.buttonSkill;

        attack.functions = buttonAtk;
        attack.anim = anim;
        attack.source = source;
    }

    void LateUpdate()
    {
        Vector3 centerPos = Vector3.zero;
        centerPos.y = animYPos;
        anim.transform.localPosition = centerPos;
        anim.transform.localEulerAngles = Vector3.zero;
    }
}
