﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMap : MonoBehaviour
{
    public Scenarios[] scenarios;

    [SerializeField] Image mapImage;
    [SerializeField] GameObject popUpMaps;
    [SerializeField] BoxCollider SelectHero;

    public static Scenarios currentMap;

    void Start()
    {        
        currentMap = scenarios[0];
        mapImage.sprite = currentMap.sprite;

        for(int i = 0; i < scenarios.Length; i++)
        {
            scenarios[i].scenario.SetActive(false);
        }
    }

    public void abrePopUp()
    {
        popUpMaps.SetActive(true);
        SelectHero.enabled = false;
    }

    public void Map1()
    {
        if(currentMap.sprite != scenarios[0].sprite)
        {
            currentMap = scenarios[0];
            mapImage.sprite = currentMap.sprite;
            popUpMaps.SetActive(false);
            SelectHero.enabled = true;
        }
    }

    public void Map2()
    {
        if (currentMap.sprite != scenarios[1].sprite)
        {
            currentMap = scenarios[1];
            mapImage.sprite = currentMap.sprite;
            popUpMaps.SetActive(false);
            SelectHero.enabled = true;
        }
    }

    public void ClosePopUp()
    {
        popUpMaps.SetActive(false);
        SelectHero.enabled = true;
    }
}
[System.Serializable]
public class Scenarios
{
    public GameObject scenario;
    public Transform bornPlace;
    public Transform hidingPlace;
    public Sprite sprite;
}
