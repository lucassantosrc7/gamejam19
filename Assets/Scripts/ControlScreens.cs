﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScreens : MonoBehaviour
{

    public static ControlScreens instance { get; private set; }

    public enum Screens
    {
        Game, Menu, Store
    }
    [SerializeField]
    Screens first_Screen;

    public static Screens currentScreen;

    [Header("Screens")]
    public GameObject menu;
    public GameObject game, store;

    AudioSource source;

    [SerializeField]
    AudioClip buttonClip;

    void Start()
    {
        instance = this;
        source = GetComponent<AudioSource>();

        ChangeScreen(first_Screen);
    }

    public void ChangeScreen(Screens newScreen)
    {
        switch (currentScreen)
        {
            case Screens.Menu:
                menu.SetActive(false);
                break;
            case Screens.Game:
                game.SetActive(false);
                break;
            case Screens.Store:
                store.SetActive(false);
                break;
        }

        switch (newScreen)
        {
            case Screens.Menu:
                menu.SetActive(true);
                break;
            case Screens.Game:
                game.SetActive(true);
                break;
            case Screens.Store:
                store.SetActive(true);
                break;
        }

        currentScreen = newScreen;
    }

    public void GoToGame()
    {
        if (buttonClip != null && !source.isPlaying)
        {
            source.PlayOneShot(buttonClip);
        }
        ChangeScreen(Screens.Game);
    }
    public void GoToMenu()
    {
        if (buttonClip != null && !source.isPlaying)
        {
            source.PlayOneShot(buttonClip);
        }
        ChangeScreen(Screens.Menu);
    }
    public void GoToStore()
    {
        if (buttonClip != null && !source.isPlaying)
        {
            source.PlayOneShot(buttonClip);
        }
        ChangeScreen(Screens.Store);
    }
}
