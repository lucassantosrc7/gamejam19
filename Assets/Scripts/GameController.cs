﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }
    public static GameObject player;

    public static Scenarios scenario;

    public GameObject [] playersPrefabs;

    [Header("Player Config")]
    public EventTriggerFunctions buttonAtk;
    public EventTriggerFunctions buttonMove, buttonSkill;

    public Transform circle;

    
    public GameObject[] hudNPC, hudPlayer;

    void Awake()
    {
        Instance = GetComponent<GameController>();

        if (player == null) player = FindTheHero();
        scenario = SelectMap.currentMap;
        scenario.scenario.SetActive(true);

        player = Instantiate(player, scenario.bornPlace.position, scenario.bornPlace.rotation);
    }

    void Update()
    {
        
    }

    GameObject FindTheHero()
    {
        for(int i = 0; i < playersPrefabs.Length; i++)
        {
            if (playersPrefabs[i].CompareTag(SelectHero.currentCharacter.tag))
            {
                return playersPrefabs[i];
            }
        }

        return null;
    }
}
