﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buffalo : MonoBehaviour
{
    float t;
    Vector3 startPosition;
    Vector3 target;
    float timeToReachTarget;

    EventTriggerFunctions functions;

    [SerializeField]
    float distance;

    [Header("Time")]
    [SerializeField]
    float attack;
    [SerializeField]
    float anticipation;

    bool attacking = false;

    [SerializeField]
    AudioClip[] clipsUlt;

    AudioSource source;

    void Start()
    {
        functions = GameController.Instance.buttonSkill;
        source = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (attacking)
        {
            float disTarget = Vector3.Distance(transform.position, target);
            if (disTarget <= 0.2f)
            {
                attacking = false;
            }
            else
            {
                t += Time.deltaTime / timeToReachTarget;
                transform.position = Vector3.Lerp(startPosition, target, t);
            }
        }
        else if (functions.state == EventTriggerFunctions.Functions.Down)
        {
            if (clipsUlt.Length > 0 && !source.isPlaying)
            {
                source.PlayOneShot(clipsUlt[Random.Range(0, clipsUlt.Length)]);
            }
            Player.anim.SetBool("Ult", true);
            functions.state = EventTriggerFunctions.Functions.Null;

            StartCoroutine(Anticipation());
        }
        else
        {
            Player.anim.SetBool("Ult", false);
        }
    }
    public void SetDestination(Vector3 destination, float time)
    {
        t = 0;
        startPosition = transform.position;
        timeToReachTarget = time;
        target = destination;

        attacking = true;
    }

    IEnumerator Anticipation()
    {
        yield return new WaitForSeconds(anticipation);
        Vector3 destination = transform.position;
        destination += transform.TransformDirection(Vector3.forward * distance);
        SetDestination(destination, attack);
    }
}
