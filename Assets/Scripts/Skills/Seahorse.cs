﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seahorse : MonoBehaviour
{
    [SerializeField]
    float distanceBack = 2;

    [SerializeField]
    float distance = 2;

    EventTriggerFunctions functions;

    [SerializeField]
    AudioClip[] clipsUlt;

    AudioSource source;

    [SerializeField]
    ParticleSystem water;

    void Start()
    {
        functions = GameController.Instance.buttonSkill;
        source = GetComponent<AudioSource>();

        water.Stop();
    }

    void Update()
    {
        if (functions.state == EventTriggerFunctions.Functions.Down)
        {
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance))
            {
                object[] objs = new object[] { distanceBack, ForceMode.Acceleration };
                hit.transform.SendMessage("Hit", objs, SendMessageOptions.DontRequireReceiver);
            }

            water.Play();
            if (clipsUlt.Length > 0 && !source.isPlaying)
            {
                source.PlayOneShot(clipsUlt[Random.Range(0, clipsUlt.Length)]);
            }
            Player.anim.SetBool("Ult", true);
            functions.state = EventTriggerFunctions.Functions.Null;
        }
        else
        {
            Player.anim.SetBool("Ult", false);
        }
    }
}
