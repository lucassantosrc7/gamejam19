﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField]
    float distance = 2;

    public float distanceBack = 2;

    bool atkAnim = false;

    [HideInInspector]
    public EventTriggerFunctions functions;
    [HideInInspector]
    public Animator anim;

    [SerializeField]
    AudioClip[] clipsHit, clipsMiss;

    [HideInInspector]
    public AudioSource source;

    void Update()
    {
        if (functions.state == EventTriggerFunctions.Functions.Down)
        {
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance))
            {
                object[] objs = new object[] { distanceBack, ForceMode.Impulse };
                hit.transform.SendMessage("Hit", objs, SendMessageOptions.DontRequireReceiver);

                if (clipsHit.Length > 0 && !source.isPlaying)
                {
                    source.PlayOneShot(clipsHit[Random.Range(0, clipsHit.Length)]);
                }
            }

            if(clipsMiss.Length > 0 && !source.isPlaying)
            {
                source.PlayOneShot(clipsMiss[Random.Range(0, clipsMiss.Length)]);
            }
            anim.SetBool("Atk", true);
            functions.state = EventTriggerFunctions.Functions.Null;
        }
        else
        {
            anim.SetBool("Atk", false);
        }
    }
}
