﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    [SerializeField] GameObject Menu_Principal;
    [SerializeField] GameObject Loja;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void play()
    {
        SceneManager.LoadScene("Game");
    }

    public void loja()
    {
        Menu_Principal.SetActive(false);
        Loja.SetActive(true);
    }

    public void voltarMenu()
    {
        Menu_Principal.SetActive(true);
        Loja.SetActive(false);
    }
}
